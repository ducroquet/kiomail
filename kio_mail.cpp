#include "kio_mail.h"
#include <QCoreApplication>
#include <QDebug>
#include <QTimer>

#include <AkonadiCore/Collection>
#include <AkonadiCore/EntityTreeModel>
#include <AkonadiCore/ItemFetchJob>
#include <AkonadiCore/ItemFetchScope>
#include <AkonadiCore/Monitor>
#include <AkonadiCore/Session>
#include <KMime/KMime/Content>
#include <KMime/KMime/Message>

#include <boost/shared_ptr.hpp>

typedef QSharedPointer<KMime::Message> MessagePtr;

class KIOPluginForMetaData : public QObject
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.kde.kio.slave.mail" FILE "kio_mail.json")
};

extern "C"
{
    int Q_DECL_EXPORT kdemain(int argc, char **argv)
    {
        
        QCoreApplication app(argc, argv);
        app.setApplicationName(QStringLiteral("kio_mail"));

        qDebug() << "Launching KIO slave.";
        if (argc != 4) {
            fprintf(stderr, "Usage: kio_mail protocol domain-socket1 domain-socket2\n");
            exit(-1);
        }
        
        
        KioMail slave(argv[2], argv[3]);
        QEventLoop loop;
        QObject::connect(&slave, &KioMail::ready, &loop, &QEventLoop::quit);
        loop.exec();
        slave.dispatchLoop();
        return 0;
    }
}

void KioMail::get(const QUrl &url)
{
    qDebug() << "Entering get for " << url << " ==> " << url.path();
    QString path = url.path();
    auto parts = path.split('/');
    if (parts[parts.size() - 2].endsWith(".eml")) {
        int itemId = parts[parts.size() - 2].left(parts[parts.size() - 2].indexOf('.')).toInt();
        Akonadi::ItemFetchJob *job = new Akonadi::ItemFetchJob(QList<Akonadi::Item::Id>() << itemId);
        QEventLoop testLoop;
        qDebug() << "Searching for item " << itemId;
        
        QObject::connect(job, &KJob::result, [&testLoop, &parts, this](KJob* job) {
            qDebug() << "Item found";
            if (job->error()) {
                qDebug() << "Error occurred";
                return;
            }
            Akonadi::ItemFetchJob *fetchJob = qobject_cast<Akonadi::ItemFetchJob*>(job);
            const Akonadi::Item::List items = fetchJob->items();
            foreach (const Akonadi::Item &item, items) {
                const auto msg = item.payload<MessagePtr>();
                
                const KMime::Content::List attachments = msg->attachments();
                foreach ( KMime::Content *attachment, attachments ) {
                    if (attachment->contentDisposition()->filename() == parts[parts.size() - 1]) {
                        mimeType(QString::fromLatin1(attachment->contentType()->mimeType()));
                        data(attachment->decodedContent());
                        finished();
                        return;
                    }
                }
            }
            testLoop.quit();
        });
        job->fetchScope().fetchFullPayload();
        testLoop.exec();
    } else if (url.path() == "/") {
        mimeType("inode/directory");
        finished();
    } else {
        mimeType("text/plain");
        QByteArray str("Hello world!\n");
        data(str);
        finished();
    }
    qDebug() << "Leaving get";
}

void KioMail::listDir(const QUrl &url)
{
    qDebug() << "Entering listDir for " << url;
    KIO::UDSEntryList myList;
    if (url.path() == "/") {
        for (int i = 0 ; i < treeModel->rowCount() ; i++) {
            auto idx = treeModel->index(i, 0);
            KIO::UDSEntry entry;
            auto coll = treeModel->data(idx, Akonadi::EntityTreeModel::Roles::CollectionRole).value<Akonadi::Collection>();
            entry.fastInsert(KIO::UDSEntry::UDS_NAME, QString::number(coll.id()));
            entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, treeModel->data(idx).toString());
            entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, "inode/directory");
            entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
            entry.fastInsert(KIO::UDSEntry::UDS_SIZE, treeModel->rowCount(idx));
            myList.append(entry);
        }
    } else if (url.fileName().endsWith(".eml")) {
        int itemId = url.fileName().left(url.fileName().indexOf('.')).toInt();
        Akonadi::ItemFetchJob *job = new Akonadi::ItemFetchJob(QList<Akonadi::Item::Id>() << itemId);
        QEventLoop testLoop;
        
        QObject::connect(job, &KJob::result, [&testLoop, &myList](KJob* job) {
            if (job->error()) {
                qDebug() << "Error occurred";
                return;
            }
            Akonadi::ItemFetchJob *fetchJob = qobject_cast<Akonadi::ItemFetchJob*>(job);
            const Akonadi::Item::List items = fetchJob->items();
            foreach (const Akonadi::Item &item, items) {
                const auto msg = item.payload<MessagePtr>();
                
                const KMime::Content::List attachments = msg->attachments();
                foreach ( KMime::Content *attachment, attachments ) {
                    const QString fileName = attachment->contentDisposition()->filename();
                    qDebug() << fileName;
                    
                    KIO::UDSEntry entry;
                    entry.fastInsert(KIO::UDSEntry::UDS_NAME, fileName);
                    entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, fileName);
                    entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, QString::fromLatin1(attachment->contentType()->mimeType()));
                    entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFREG);
                    entry.fastInsert(KIO::UDSEntry::UDS_SIZE, attachment->size());
                    myList.append(entry);
                }
            }
            testLoop.quit();
        });
        job->fetchScope().fetchFullPayload();
        testLoop.exec();
    } else {
        int collectionId = url.fileName().toInt();
        Akonadi::Collection coll(collectionId);
        auto collIdx = Akonadi::EntityTreeModel::modelIndexForCollection(treeModel, coll);
        for (int i = 0 ; i < treeModel->rowCount(collIdx) ; i++) {
            auto idx = treeModel->index(i, 0, collIdx);
            KIO::UDSEntry entry;
            auto coll = treeModel->data(idx, Akonadi::EntityTreeModel::Roles::CollectionRole).value<Akonadi::Collection>();
            entry.fastInsert(KIO::UDSEntry::UDS_NAME, QString::number(coll.id()));
            entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, treeModel->data(idx).toString());
            entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, "inode/directory");
            entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
            entry.fastInsert(KIO::UDSEntry::UDS_SIZE, treeModel->rowCount(idx));
            myList.append(entry);
        }
        
        
        Akonadi::ItemFetchJob *job = new Akonadi::ItemFetchJob(coll);
        QEventLoop testLoop;
        
        QObject::connect(job, &KJob::result, [&testLoop, &myList](KJob* job) {
            if (job->error()) {
                qDebug() << "Error occurred";
                return;
            }
            Akonadi::ItemFetchJob *fetchJob = qobject_cast<Akonadi::ItemFetchJob*>(job);
            const Akonadi::Item::List items = fetchJob->items();
            foreach (const Akonadi::Item &item, items) {
                if (item.hasFlag("$ATTACHMENT")) {
                    const auto msg = item.payload<MessagePtr>();
                    
                    KIO::UDSEntry entry;
                    entry.fastInsert(KIO::UDSEntry::UDS_NAME, QString("%1.eml").arg(item.id()));
                    entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, msg->subject()->asUnicodeString());
                    entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, "inode/directory");
                    entry.fastInsert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
                    entry.fastInsert(KIO::UDSEntry::UDS_SIZE, msg->attachments().size());
                    myList.append(entry);
                }
            }
            testLoop.quit();
        });
        job->fetchScope().fetchFullPayload();
        testLoop.exec();
    }
    listEntries(myList);
    finished();
    qDebug() << "Leaving listDir";
}

void KioMail::mimetype(const QUrl &url)
{
    qDebug() << "Calling mimetype for " << url;
    if (url.path() == "/")
        mimeType("inode/directory");
    else
        mimeType("text/plain");
    finished();
}

void KioMail::stat(const QUrl &url)
{
    qDebug() << "Calling stat for " << url;
    KIO::UDSEntry entry;
    QString path = url.path();
    auto parts = path.split('/');
    if (parts.size() > 3 && parts[parts.size() - 2].endsWith(".eml")) {
        int itemId = parts[parts.size() - 2].left(parts[parts.size() - 2].indexOf('.')).toInt();
        Akonadi::ItemFetchJob *job = new Akonadi::ItemFetchJob(QList<Akonadi::Item::Id>() << itemId);
        QEventLoop testLoop;
        
        QObject::connect(job, &KJob::result, [&testLoop, &parts, &entry](KJob* job) {
            if (job->error()) {
                qDebug() << "Error occurred";
                return;
            }
            Akonadi::ItemFetchJob *fetchJob = qobject_cast<Akonadi::ItemFetchJob*>(job);
            const Akonadi::Item::List items = fetchJob->items();
            foreach (const Akonadi::Item &item, items) {
                const auto msg = item.payload<MessagePtr>();
                
                const KMime::Content::List attachments = msg->attachments();
                foreach ( KMime::Content *attachment, attachments ) {
                    if (attachment->contentDisposition()->filename() == parts[parts.size() - 1]) {
                        entry.fastInsert(KIO::UDSEntry::UDS_NAME, attachment->contentDisposition()->filename());
                        entry.fastInsert(KIO::UDSEntry::UDS_DISPLAY_NAME, attachment->contentDisposition()->filename());
                        entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, QString::fromLatin1(attachment->contentType()->mimeType()));
                        entry.fastInsert(KIO::UDSEntry::UDS_SIZE, attachment->decodedContent().size());
                        qDebug() << "Infos inserted";
                        break;
                    }
                }
            }
            testLoop.quit();
        });
        job->fetchScope().fetchFullPayload();
        testLoop.exec();
    } else {
        entry.fastInsert(KIO::UDSEntry::UDS_MIME_TYPE, "inode/directory");
    }
    statEntry(entry);
    finished();
    qDebug() << "stat finished";
}

KioMail::KioMail(const QByteArray &pool, const QByteArray &app)
    : SlaveBase("mail", pool, app) {
    Akonadi::Session *session = new Akonadi::Session( "kioMail" );
    Akonadi::Monitor *monitor = new Akonadi::Monitor(  );
    monitor->setCollectionMonitored( Akonadi::Collection::root() );
    monitor->setMimeTypeMonitored( "message/rfc822" );
    monitor->setSession( session );
    treeModel = new Akonadi::EntityTreeModel( monitor );
    connect(treeModel, &Akonadi::EntityTreeModel::collectionTreeFetched, this, &KioMail::ready);
}

#include "kio_mail.moc"
