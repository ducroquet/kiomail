#ifndef KIOMAIL_H
#define KIOMAIL_H

#include <kio/slavebase.h>

namespace Akonadi {
    class Session;
    class Monitor;
    class EntityTreeModel;
    class Collection;
};

/**
  This class implements a Mail kioslave
 */
class KioMail : public QObject, public KIO::SlaveBase
{
    Q_OBJECT
public:
    KioMail(const QByteArray &pool, const QByteArray &app);
    void get(const QUrl &url) Q_DECL_OVERRIDE;
    void listDir(const QUrl &url) Q_DECL_OVERRIDE;
    void mimetype(const QUrl &url) Q_DECL_OVERRIDE;
    void stat(const QUrl &url) Q_DECL_OVERRIDE;
signals:
    void ready();
private:
    Akonadi::EntityTreeModel *treeModel;
};

#endif
